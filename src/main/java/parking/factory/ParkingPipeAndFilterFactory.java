package parking.factory;

import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import parking.loader.ClassConfiguration;
import parking.loader.Loader;

public class ParkingPipeAndFilterFactory {

	public Map<String, Object> getParkingCore(Properties properties) {

		ClassConfiguration configs = new ClassConfiguration(properties.getProperty("json.config.file"));
		Loader loader = new Loader(properties);
		try {
			loader.loadClasses(configs.getConfiguration());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// build the parking core
		return loader.getInstances();
	}

}
