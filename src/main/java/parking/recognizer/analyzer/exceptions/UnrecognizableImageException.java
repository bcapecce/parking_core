package parking.recognizer.analyzer.exceptions;

public class UnrecognizableImageException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public UnrecognizableImageException() {
        super("The analyzed image is not recognizable.");
    }
}
