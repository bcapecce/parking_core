package parking.recognizer.analyzer;

import parking.recognizer.analyzer.AnalysisResult;

import java.awt.*;

public interface Analyzer {
    AnalysisResult analyze(Image image);
}
