package parking.recognizer.analyzer;

/**
 * Class in charge of storing the data obtained from the analyzers
 */
public class AnalysisResult {

	private String licenseNumber;
	private double accuracy;

	public AnalysisResult(String licenseNumber, double accuracy) {
		this.licenseNumber = licenseNumber;
		this.accuracy = accuracy;
	}

	public String getLicenseNumber() {
		return licenseNumber;
	}

	public double getAccuracy() {
		return accuracy;
	}
}
