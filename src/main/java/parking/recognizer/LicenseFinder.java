package parking.recognizer;

import java.awt.Image;
import java.util.List;

import parking.filter.Filter;
import parking.filter.Next;
import parking.filter.Result;
import parking.recognizer.analyzer.AnalysisResult;
import parking.recognizer.analyzer.Analyzer;

public class LicenseFinder extends Filter<Image, String> {

	private final List<Analyzer> analyzers;
	private final Double minAccuracy;

	public LicenseFinder(Next<String> next, List<Analyzer> analyzers, String minAccuracy) {
		super(next);
		this.analyzers = analyzers;
		this.minAccuracy = Double.parseDouble(minAccuracy);
	}

	@Override
	protected Result<String> transformBetween(Image input) {
		for (Analyzer analyzer : analyzers) {
			AnalysisResult result = analyzer.analyze(input);
			if (result.getAccuracy() >= minAccuracy)
				return new Result<String>(true, result.getLicenseNumber(), null);
		}
		return new Result<String>(false, null, "The analyzed image is not recognizable.");
	}
}
