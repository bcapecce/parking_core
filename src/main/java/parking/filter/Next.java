package parking.filter;

public interface Next<I> {
	String process(I input);
}
