package parking.filter;

public abstract class Source<O> {
	private Next<O> next;

	public Source(Next<O> next) {
		this.next = next;
	}

	public String process(O output) {
		String result = null;
		try {
			result = this.next.process(output);
		} catch (RuntimeException e) {
			return e.getMessage();
		}
		return result;
	}
}
