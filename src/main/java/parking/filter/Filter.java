package parking.filter;

public abstract class Filter<I, O> implements Next<I> {

	Next<O> next;

	public Filter(Next<O> next) {
		this.next = next;
	}

	public String process(I input) {
		Result<O> result = transformBetween(input);
		if (result.shouldContinue()) {
			return next.process(result.getObject());
		}
		return result.getMessage();
	}

	protected abstract Result<O> transformBetween(I input);
}
