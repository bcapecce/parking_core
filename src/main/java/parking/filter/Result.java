package parking.filter;

public class Result<O> {

	private Boolean ok;
	private O object;
	private String message;

	public Result(Boolean ok, O object, String message) {
		this.ok = ok;
		this.object = object;
		this.message = message;
	}

	public Boolean shouldContinue() {
		return ok;
	}

	public O getObject() {
		return object;
	}

	public String getMessage() {
		return message;
	}
}
