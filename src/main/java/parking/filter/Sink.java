package parking.filter;

public abstract class Sink<I> implements Next<I> {

	public abstract String process(I input);
}
