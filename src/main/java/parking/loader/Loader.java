package parking.loader;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import parking.recognizer.analyzer.Analyzer;

/**
 * The loader is responsible for loading the objects found in a certain
 * directory and in the configuration
 */
public class Loader {

	private Map<String, Object> instances;
	private Map<String, List<Object>> listInstances;
	private Properties properties;
	private final static String CLASS_PATH = "parking.analyzer.license.";

	public Loader(Properties properties) {
		this.properties = properties;
		this.instances = new HashMap<>();
		this.listInstances = new HashMap<>();
	}

	/**
	 * loads the classes found in the directory
	 */
	public void findClasses(String path) {

		for (File file : new File(path).listFiles()) {
			try {
				if (!file.getName().endsWith(".class")) {
					continue;
				}

				Class<?> c = Class.forName(CLASS_PATH + file.getName().replace(".class", ""));
				if (!Analyzer.class.isAssignableFrom(c)) {
					throw new RuntimeException("the class " + file.getName() + "does not implement Analyzer");
				}
				instances.put(c.getName(), c.newInstance());
			} catch (Exception e) {
				throw new RuntimeException("error loading class " + file.getName(), e);
			}

		}
	}

	/**
	 * loads the classes found in config file
	 */
	public void loadClasses(List<Config> configs) {
		try {
			for (Config config : configs) {
				if (config.getType().equals("list")) {
					List<Object> listObjects = new ArrayList<>();
					for (Config innerConfig : config.getList()) {
						Class<?> classToIntence = Class.forName(innerConfig.getName());
						Object object = instance(innerConfig, classToIntence);
						listObjects.add(object);
					}
					listInstances.put(config.getId(), listObjects);
				} else if (config.getType().equals("class")) {
					Class<?> classToIntence = Class.forName(config.getName());
					Object object = instance(config, classToIntence);
					instances.put(config.getId(), object);
				}
			}
		} catch (Exception e) {
			throw new RuntimeException("error loading classes", e);
		}
	}

	/**
	 * return the map with the objects instantiated
	 * 
	 * @return the map with the objects instantiated
	 */
	public Map<String, Object> getInstances() {
		return instances;
	}

	/**
	 * instance the classToIntence with the parameters configured
	 * 
	 * @return the object instantiated
	 */
	private Object instance(Config config, Class<?> classToIntence) throws ClassNotFoundException,
			InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {

		Class<?>[] cArg = new Class[config.getArgs().size()];
		Object[] ins = new Object[config.getArgs().size()];

		int i = 0;

		for (ArgConfig arg : config.getArgs()) {
			if (arg.getType().equals("class")) {
				cArg[i] = Class.forName(arg.getName());
				ins[i] = instances.get(arg.getValue());
			} else if (arg.getType().equals("list")) {
				cArg[i] = Class.forName("java.util.List");
				ins[i] = listInstances.get(arg.getValue());
			} else if (arg.getType().equals("property")) {
				cArg[i] = Class.forName("java.lang.String");
				ins[i] = properties.getProperty(arg.getValue());
			}
			i++;
		}
		return classToIntence.getDeclaredConstructor(cArg).newInstance(ins);
	}
}
