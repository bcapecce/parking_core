package parking.loader;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ClassConfiguration {
	private String fileName;

	public ClassConfiguration(String fileName) {
		this.fileName = fileName;
	}

	/**
	 * gets the config from the file which is in the indicated directory
	 * 
	 * @return the list configs
	 */
	public List<Config> getConfiguration() throws IOException, JsonParseException, JsonMappingException {
		File fi = new File(fileName);
		ObjectMapper o = new ObjectMapper();

		List<Config> configs = o.readValue(fi, new TypeReference<List<Config>>() {
		});
		return configs;
	}

}
