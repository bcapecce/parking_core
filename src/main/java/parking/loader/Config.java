package parking.loader;

import java.util.List;

public class Config {

	private String id;
	private String type;
	private String name;
	private List<ArgConfig> args;
	private List<Config> list;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ArgConfig> getArgs() {
		return args;
	}

	public void setArgs(List<ArgConfig> args) {
		this.args = args;
	}

	public List<Config> getList() {
		return list;
	}

	public void setList(List<Config> list) {
		this.list = list;
	}

}
