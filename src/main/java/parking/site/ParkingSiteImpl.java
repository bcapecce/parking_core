package parking.site;

import java.util.ArrayList;
import java.util.List;

public class ParkingSiteImpl implements ParkingSite {
	private List<Observer> observers;
	private int availablePlaces;

	public ParkingSiteImpl(String availablePlaces) {
		this.availablePlaces = Integer.parseInt(availablePlaces);
		this.observers = new ArrayList<>();
	}

	public int getAvailablePlaces() {
		return availablePlaces;
	}

	@Override
	public void attach(Observer observer) {
		observers.add(observer);
	}

	@Override
	public void detach(Observer observer) {
		observers.remove(observer);
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean hasPlaceAvailable() {
		return true;
	}

	@Override
	public void registerVehicle(String licenseNumber) {
		notifyObservers();
	}

	private void notifyObservers() {
		observers.forEach(observer -> observer.update());
	}

}
