package parking.site;

public class Persona {
	String nombre;
	String apellido;
	int idPersona;
	String email;
	String numeroTelefono;
	
	public Persona(String nombre, String apellido, int idPersona,String email, String numero) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.idPersona = idPersona;
		this.email = email;
		this.numeroTelefono = numero;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	
	public int getIdPersona() {
		return idPersona;
	}

	public String getNumeroTelefono() {
		return numeroTelefono;
	}
	
	public String getEmail() {
		return email;
	}
	
}
