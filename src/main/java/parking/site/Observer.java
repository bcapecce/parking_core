package parking.site;

public interface Observer {
  public void update();
}
