package parking.site;

public enum ValidationResult {
	OK, NOT_AUTHORIZED, NO_PLACE_AVAILABLE, DISABLED_PARKING, EXISTING_VEHICLE
}
