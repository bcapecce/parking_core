package parking.site;

public interface ParkingSite {

	void attach(Observer observer);

	void detach(Observer observer);

	boolean isEnabled();

	boolean hasPlaceAvailable();

	void registerVehicle(String licenseNumber);

}
