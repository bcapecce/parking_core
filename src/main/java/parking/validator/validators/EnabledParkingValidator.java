package parking.validator.validators;

import parking.site.ParkingSiteImpl;
import parking.site.ValidationResult;
import parking.validator.validators.exceptions.ParkingDisabledException;

public class EnabledParkingValidator implements Validator {

	private final ParkingSiteImpl parking;

	public EnabledParkingValidator(ParkingSiteImpl parking) {
		this.parking = parking;
	}

	@Override
	public ValidationResult validate(String licenseNumber){
		if (!parking.isEnabled()) throw new ParkingDisabledException();
		return ValidationResult.OK;
	}
}
