package parking.validator.validators.exceptions;

public class NoPlaceAvailableException extends RuntimeException {
	public NoPlaceAvailableException() {
		super("No place available in the parking.");
	}
}
