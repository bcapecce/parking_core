package parking.validator.validators.exceptions;

public class VehicleExistsException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VehicleExistsException(String licenseNumber) {
		super(String.format("Vehicle %s is parked.", licenseNumber));
	}
}
