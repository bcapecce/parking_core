package parking.validator.validators.exceptions;

public class ParkingDisabledException extends RuntimeException {
	public ParkingDisabledException() { super("Parking lot currently disabled."); }
}
