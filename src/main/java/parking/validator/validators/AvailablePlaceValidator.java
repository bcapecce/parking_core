package parking.validator.validators;

import parking.site.ParkingSiteImpl;
import parking.site.ValidationResult;
import parking.validator.validators.exceptions.NoPlaceAvailableException;

/**
 * Validator responsible for checking the existence of available parking space
 * for a vehicle
 */
public class AvailablePlaceValidator implements Validator {

	private final ParkingSiteImpl parking;

	public AvailablePlaceValidator(ParkingSiteImpl parking) {
		this.parking = parking;
	}

	@Override
	public ValidationResult validate(String licenseNumber) {
		if (!parking.hasPlaceAvailable())
			throw new NoPlaceAvailableException();
		return ValidationResult.OK;
	}
}
