package parking.validator.validators;

import parking.site.ParkedVehicles;
import parking.site.ValidationResult;
import parking.validator.validators.exceptions.VehicleExistsException;

/**
 * Validator responsible for checking if an incoming vehicle is already inside
 * the parking lot
 */
public class ParkedValidator implements Validator {

	private final ParkedVehicles parkedVehicles;

	public ParkedValidator(ParkedVehicles parkedVehicles) {
		this.parkedVehicles = parkedVehicles;
	}

	@Override
	public ValidationResult validate(String licenseNumber) {
		if (parkedVehicles.licenseExists(licenseNumber))
			throw new VehicleExistsException(licenseNumber);
		return ValidationResult.OK;
	}
}
