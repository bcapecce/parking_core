package parking.validator.validators;

import java.util.ArrayList;
import java.util.List;

import parking.site.ValidationResult;

public class CompositeValidator implements Validator {

	private List<Validator> childValidators;

	public CompositeValidator(List<Validator> validators) {
		this.childValidators = new ArrayList<>();
		this.childValidators = validators;
	}

	public ValidationResult validate(String licenseNumber) {
		for (Validator validator : childValidators) {
			ValidationResult result = validator.validate(licenseNumber);
			if (!result.equals(ValidationResult.OK))
				return result;
		}
		return ValidationResult.OK;
	}

}
