package parking.validator.validators;

import parking.site.ValidationResult;

public interface Validator {
	ValidationResult validate(String licenseNumber);
}
