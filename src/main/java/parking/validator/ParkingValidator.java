package parking.validator;

import parking.filter.Filter;
import parking.filter.Next;
import parking.filter.Result;
import parking.notification.MessageGateway;
import parking.site.ValidationResult;
import parking.validator.validators.Validator;

public class ParkingValidator extends Filter<String, String> {
	private final Validator validator;
	private final MessageGateway gateway;

	public ParkingValidator(Next<String> next, Validator validator, MessageGateway gateway) {
		super(next);
		this.validator = validator;
		this.gateway = gateway;
	}

	@Override
	protected Result<String> transformBetween(String licenseNumber) {
		ValidationResult validationResult = validator.validate(licenseNumber);

		if (!ValidationResult.OK.equals(validationResult)) {
			gateway.notifyAdm(validationResult);
		}
		return new Result<String>(ValidationResult.OK.equals(validationResult), validationResult.name(), "");
	}
}
