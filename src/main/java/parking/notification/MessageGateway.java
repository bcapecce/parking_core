package parking.notification;

import parking.site.ValidationResult;

public class MessageGateway {

	private Sender senderSms;
	private Sender senderWhat;
	private String admPhone;
	private String service;

	public MessageGateway(Sender senderSms, Sender senderWhat, String service, String admPhone) {
		this.senderSms = senderSms;
		this.senderWhat = senderWhat;
		this.admPhone = admPhone;
		this.service = service;
	}

	public Boolean notifyAdm(ValidationResult result) {
		String message = NotificationSelector.getMessage(result);
		if ("sms".equals(service)) {
			return senderSms.send(admPhone, message);
		}
		return senderWhat.send(admPhone, message);
	}
}
