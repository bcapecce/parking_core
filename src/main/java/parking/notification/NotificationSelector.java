package parking.notification;

import parking.site.ValidationResult;

public class NotificationSelector {
	//1= Sin lugares disponibles - NO_PLACE_AVAILABLE
	//2= Estacionamiento Desabilitado - DISABLED_PARKING
	//3= Existe automovil - EXISTING_VEHICL

	
	public static String getMessage(ValidationResult evento) {
		if(evento.equals(ValidationResult.NO_PLACE_AVAILABLE)) {
			return "App - Parking Spot: En el estacionamiento no hay espacios disponibles.";
		}
		else if(evento.equals(ValidationResult.DISABLED_PARKING)) {
			return "App - Parking Spot: El estacionamiento no esta habilitado.";
		}
		else if(evento.equals(ValidationResult.EXISTING_VEHICLE)) {
			return "App - Parking Spot: En el estacionamiento ya se registro un vehivulo con esa patente.";
		}
		return "App - Parking Spot";
	}

}
