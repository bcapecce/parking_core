package parking.notification;

public interface Sender {
	boolean send(String number, String message);
}
