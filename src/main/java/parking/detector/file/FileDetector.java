package parking.detector.file;

import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import parking.filter.Next;
import parking.filter.Source;

public class FileDetector extends Source<Image> implements Runnable, FileChecker {

	private String path;
	private int sleepInMiliSeconds;
	private boolean run = false;

	public FileDetector(Next<Image> next, String path, String sleepInMiliSeconds) {
		super(next);
		this.path = path;
		this.sleepInMiliSeconds = Integer.parseInt(sleepInMiliSeconds);
	}

	public void detect() throws IOException, InterruptedException {
		while (run) {
			try {
				for (File file : new File(path).listFiles()) {
					if (!file.getName().endsWith(".jpg")) {
						continue;
					}
					FileInputStream fis = new FileInputStream(file);
					this.process(ImageIO.read(fis));
					file.delete();
				}
			} catch (Exception e) {
				System.out.println("Error");
				e.printStackTrace();
			}
			Thread.sleep(this.sleepInMiliSeconds);
		}
	}

	@Override
	public void run() {
		try {
			this.detect();
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start() {
		this.run = true;
		new Thread(this).start();
	}

	@Override
	public void stop() {
		if (this.run) {
			this.run = false;
		} else {
			System.out.println("File detector not running");
		}
	}

}
