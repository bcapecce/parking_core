package parking.detector.image;

import java.awt.Image;
import java.util.Comparator;
import java.util.Objects;

import parking.filter.Filter;
import parking.filter.Next;
import parking.filter.Result;

public class ImageChangeDetector extends Filter<Image, Image> {
	private Image lastImage;

	private int threshold;

	private Comparator<Image> imageComparator;

	public ImageChangeDetector(Next<Image> next, Comparator<Image> imageComparator, String threshold) {
		super(next);
		this.imageComparator = imageComparator;
		this.threshold = Integer.parseInt(threshold);
	}

	@Override
	protected Result<Image> transformBetween(Image input) {
		Image tmpImage = lastImage;
		lastImage = input;
		if (Objects.isNull(tmpImage) || imageComparator.compare(tmpImage, input) > threshold) {
			return new Result<Image>(true, input, null);
		}
		return new Result<Image>(false, input, "equal image");
	}

	public void setLastImage(Image lastImage) {
		this.lastImage = lastImage;
	}

}
