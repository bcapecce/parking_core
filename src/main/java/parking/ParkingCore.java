package parking;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

import parking.detector.file.FileDetector;
import parking.factory.ParkingPipeAndFilterFactory;
import parking.site.ParkingSite;

public class ParkingCore {
	private Map<String, Object> instances;

	public ParkingCore(String filePropertyName) {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream(new File(filePropertyName)));
		} catch (IOException e) {
			System.err.println(String.format("Error reading file %s", filePropertyName));
		}
		ParkingPipeAndFilterFactory parkingFactory = new ParkingPipeAndFilterFactory();
		instances = parkingFactory.getParkingCore(properties);
	}

	public ParkingSite getParkingSite() {
		return (ParkingSite) instances.get("ParkingSite");
	}

	public void startPipeAndFilter() {
		FileDetector fileChecker = (FileDetector) instances.get("FileDetector");
		fileChecker.start();
	}
}
