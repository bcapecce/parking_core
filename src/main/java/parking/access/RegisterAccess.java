package parking.access;

import parking.site.ParkedVehicles;
import parking.site.ParkingSite;

public class RegisterAccess {

	private ParkedVehicles parkedVehicles;
	private ParkingSite parking;

	public RegisterAccess(ParkedVehicles parkedVehicles, ParkingSite parking) {
		this.parkedVehicles = parkedVehicles;
		this.parking = parking;
	}

	public void registerVehicle(String licenseNumber) {
		this.parking.registerVehicle(licenseNumber);
		this.parkedVehicles.registerVehicle(licenseNumber);
	}
}
