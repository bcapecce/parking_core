package parking.access.exceptions;

public class ErrorInBarrierException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ErrorInBarrierException() {
		super("Error open the barrier");
	}

}
