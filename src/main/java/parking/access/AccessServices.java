package parking.access;

import parking.access.exceptions.ErrorInBarrierException;
import parking.filter.Sink;

public class AccessServices extends Sink<String> {
	private AccessControl accessControl;
	private RegisterAccess registerAccess;

	public AccessServices(AccessControl accessControl, RegisterAccess registerAccess) {
		this.accessControl = accessControl;
		this.registerAccess = registerAccess;
	}

	public String process(String licenseNumber) {
		if (!this.accessControl.toggleBarrier()) {
			throw new ErrorInBarrierException();
		}
		this.registerAccess.registerVehicle(licenseNumber);
		return AccessResult.OK.name();
	}

}
