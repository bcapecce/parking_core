package parking.access;

public enum AccessResult {
	OK, ERROR_IN_BARRIER, ERROR_IN_REGISTER
}
