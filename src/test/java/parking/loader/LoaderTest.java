package parking.loader;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class LoaderTest {

	private String path;
	private ClassConfiguration classConfiguration;
	private Properties properties;

	@Before
	public void setup() {

		this.path = System.getProperty("user.dir") + "/src/test/resources/";
		classConfiguration = new ClassConfiguration(this.path + "class.json");

		properties = new Properties();
		properties.put("file.detector.path", path + "/images");
		properties.put("file.detector.delay", "10000");
		properties.put("file.detector.image.path.threshold", "25");
		properties.put("analyzers.min_accuracy", ".7");
		properties.put("notificator.phone", "116123323");
		properties.put("notificator.service", "SMS");
		properties.put("notificator.account", "AC940ae9d1885c70a30ed953ea95bc98cb");
		properties.put("notificator.token", "a225856ade3292c40dfa2e0ae48e4241");
		properties.put("parking.available.places", "2");

	}

	@Test
	public void getInstancessTest() throws JsonParseException, JsonMappingException, IOException {
		// given
		List<Config> configs = classConfiguration.getConfiguration();
		Loader loader = new Loader(properties);

		// when
		loader.loadClasses(configs);
		Map<String, Object> instances = loader.getInstances();

		// then
		assertEquals(14, instances.size());
	}

	@Test
	public void findClassesTest() throws JsonParseException, JsonMappingException, IOException {
		// given
		Loader loader = new Loader(properties);

		// when
		loader.findClasses(this.path);
		Map<String, Object> instances = loader.getInstances();

		// then
		assertEquals(1, instances.size());
	}

}
