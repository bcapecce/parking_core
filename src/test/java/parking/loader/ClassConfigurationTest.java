package parking.loader;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ClassConfigurationTest {

	private String path;

	@Before
	public void setup() {
		this.path = System.getProperty("user.dir") + "/src/test/resources/";
	}

	@Test
	public void getConfigTest() throws JsonParseException, JsonMappingException, IOException {
		// given
		ClassConfiguration classConfiguration = new ClassConfiguration(this.path + "class.json");

		// when
		List<Config> configs = classConfiguration.getConfiguration();

		// then
		assertEquals(16, configs.size());
	}

	@Test(expected = IOException.class)
	public void getConfigNotExistsTest() throws JsonParseException, JsonMappingException, IOException {
		// given
		ClassConfiguration classConfiguration = new ClassConfiguration(this.path + "classNotExists.json");

		// when
		classConfiguration.getConfiguration();
	}

	@Test(expected = JsonMappingException.class)
	public void getConfigErrorTest() throws JsonParseException, JsonMappingException, IOException {
		// given
		ClassConfiguration classConfiguration = new ClassConfiguration(this.path + "classError.json");

		// when
		classConfiguration.getConfiguration();
	}
}
