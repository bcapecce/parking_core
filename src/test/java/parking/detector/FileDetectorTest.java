package parking.detector;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Test;
import org.mockito.Mockito;

import parking.detector.file.FileDetector;
import parking.filter.Next;

public class FileDetectorTest {

	private static RenderedImage TEST_IMAGE_AC11111LV = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);

	@Test
	public void detecTest() throws IOException, InterruptedException {
		// given
		String path = System.getProperty("user.dir") + "/src/test/resources/";
		File outputfile = new File(path + "test.jpg");
		ImageIO.write(TEST_IMAGE_AC11111LV, "jpg", outputfile);
		Next<Image> next = Mockito.mock(Next.class);
		FileDetector fileDetector = new FileDetector(next, path, "1000000");
		// when
		fileDetector.start();
		Thread.sleep(1000);
		fileDetector.stop();

		// then
		Mockito.verify(next).process(Mockito.any());
	}

}
