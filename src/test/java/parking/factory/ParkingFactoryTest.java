package parking.factory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import static org.junit.Assert.assertTrue;

public class ParkingFactoryTest {

	private Properties properties;

	@Before
	public void setup() throws JsonParseException, JsonMappingException, IOException {
		properties = new Properties();
		properties.load(new FileInputStream(
				new File(System.getProperty("user.dir") + "/src/test/resources/parking_core.properties")));
		properties.setProperty("json.config.file", System.getProperty("user.dir") + "/src/test/resources/class.json");
	}

	@Test
	public void FactoryTest() {
		// given
		ParkingPipeAndFilterFactory parkingFactory = new ParkingPipeAndFilterFactory();

		// when
		Map<String, Object> instances = parkingFactory.getParkingCore(this.properties);

		// then
		assertTrue(Objects.nonNull(instances));
	}

}
