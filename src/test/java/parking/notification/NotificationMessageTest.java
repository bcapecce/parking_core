package parking.notification;

import org.junit.Assert;
import org.junit.Test;

import parking.notification.NotificationSelector;
import parking.site.ValidationResult;

public class NotificationMessageTest {
	
	@Test
	public void messageNoPlaceAvailableTest() {
		Assert.assertEquals(NotificationSelector.getMessage(ValidationResult.NO_PLACE_AVAILABLE),"App - Parking Spot: En el estacionamiento no hay espacios disponibles.");		
	}
	
	@Test
	public void messageDisabledParkingTest() {
		Assert.assertEquals(NotificationSelector.getMessage(ValidationResult.DISABLED_PARKING),"App - Parking Spot: El estacionamiento no esta habilitado.");		
	}
	
	@Test
	public void messageExistingVehicleTest() {
		Assert.assertEquals(NotificationSelector.getMessage(ValidationResult.EXISTING_VEHICLE),"App - Parking Spot: En el estacionamiento ya se registro un vehivulo con esa patente.");		
	}
	
}
