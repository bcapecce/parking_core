package parking.model;

import org.junit.Assert;
import org.junit.Test;

import parking.site.Persona;

public class PersonaTest {
	Persona A = new Persona("Veronica", "Juarez", 1,"leonelajuarez01@gmail.com", "1162727264");

	@Test
	public void testGetters() {
		Assert.assertEquals(A.getApellido(), "Juarez");
		Assert.assertEquals(A.getNombre(), "Veronica");
		Assert.assertEquals(A.getIdPersona(), 1);
		Assert.assertEquals("leonelajuarez01@gmail.com", A.getEmail());
		Assert.assertEquals("1162727264", A.getNumeroTelefono());
	}
	
}
