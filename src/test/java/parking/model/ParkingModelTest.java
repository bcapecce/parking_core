package parking.model;

import org.junit.Test;
import org.mockito.Mockito;

import parking.site.Observer;
import parking.site.ParkingSite;
import parking.site.ParkingSiteImpl;

public class ParkingModelTest {

	@Test
	public void attachTest() {
		// given
		ParkingSite parking = new ParkingSiteImpl("2");
		Observer observer = Mockito.mock(Observer.class);

		// when
		parking.attach(observer);
		parking.registerVehicle("licenseNumber");

		// then
		Mockito.verify(observer).update();

	}

	@Test
	public void detachTest() {
		// given
		ParkingSite parking = new ParkingSiteImpl("2");
		Observer observer = Mockito.mock(Observer.class);

		// when
		parking.attach(observer);
		parking.detach(observer);
		parking.registerVehicle("licenseNumber");

		// then
		Mockito.verify(observer, Mockito.never()).update();

	}
}
