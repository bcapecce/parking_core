package AcceptanceTests;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import parking.filter.Source;
import parking.recognizer.analyzer.AnalysisResult;
import parking.recognizer.analyzer.Analyzer;

public class UserStory3 {
	private Analyzer regularAnalyzer;
	private Analyzer lowLightAnalyzer;
	private Source<Image> fileDetector;
	private ParkingModelMock parkingModelMock;

	@Before
	public void setup() {
		parkingModelMock = new ParkingModelMock();
		parkingModelMock.setBaseScenario();

		fileDetector = parkingModelMock.getFileDetector();
		regularAnalyzer = parkingModelMock.getRegularAnalyzer();
		lowLightAnalyzer = parkingModelMock.getLowLightAnalyzer();
	}

	@Test
	public void testCA1() {
		// when
		fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		Mockito.verify(regularAnalyzer, times(1)).analyze(parkingModelMock.getImageCarA1());
		Mockito.verify(lowLightAnalyzer, never()).analyze(parkingModelMock.getImageCarA1());
	}

	@Test
	public void testCA2() {
		// given
		Mockito.when(this.regularAnalyzer.analyze(parkingModelMock.getImageCarA1()))
				.thenReturn(new AnalysisResult("", ParkingModelMock.ZERO_ACCURACY));

		// when
		String error = fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		assertEquals("The analyzed image is not recognizable.", error);
	}
}
