package AcceptanceTests;

import static org.mockito.Mockito.times;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import parking.notification.MessageGateway;
import parking.notification.Sender;
import parking.site.ValidationResult;

public class UserStory5 {

	private MessageGateway gateway;
	private Sender sender;

	@Before
	public void setup() {
		sender = Mockito.mock(Sender.class);
		gateway = new MessageGateway(sender, sender, "sms", "1162727264");
	}

	@Test
	public void testCA1() {

		// when
		gateway.notifyAdm(ValidationResult.NO_PLACE_AVAILABLE);

		// then
		Mockito.verify(sender, times(1)).send("1162727264",
				"App - Parking Spot: En el estacionamiento no hay espacios disponibles.");

	}

	@Test
	public void testCA2() {
		// give
		gateway = new MessageGateway(sender, sender, "whatsapp", "0");
		Mockito.when(sender.send("0", "App - Parking Spot: El estacionamiento no esta habilitado.")).thenReturn(false);

		// when
		Boolean result = gateway.notifyAdm(ValidationResult.EXISTING_VEHICLE);

		// then
		Assert.assertFalse(result);
	}

	@Test
	public void testCA3() {
		// give
		gateway = new MessageGateway(sender, sender, "sms", "1162727264");
		Mockito.when(sender.send("1162727264",
				"App - Parking Spot: En el estacionamiento ya se registro un vehivulo con esa patente."))
				.thenReturn(true);

		// when
		Boolean result = gateway.notifyAdm(ValidationResult.EXISTING_VEHICLE);

		// then
		Assert.assertTrue(result);
	}

	@Test
	public void testCA4() {
		// give
		gateway = new MessageGateway(sender, sender, "sms", "0");
		Mockito.when(sender.send("0", "App - Parking Spot: El estacionamiento no esta habilitado.")).thenReturn(false);

		// when
		Boolean result = gateway.notifyAdm(ValidationResult.EXISTING_VEHICLE);

		// then
		Assert.assertFalse(result);
	}

}
