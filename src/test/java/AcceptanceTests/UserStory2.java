package AcceptanceTests;

import static org.junit.Assert.assertEquals;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import parking.access.AccessControl;
import parking.analyzer.exceptions.AnalyzeCorruptImageException;
import parking.filter.Source;
import parking.recognizer.analyzer.AnalysisResult;
import parking.recognizer.analyzer.Analyzer;

public class UserStory2 {
	private Analyzer regularAnalyzer;
	private Source<Image> fileDetector;
	private AccessControl accessControl;
	private ParkingModelMock parkingModelMock;

	@Before
	public void setup() {
		parkingModelMock = new ParkingModelMock();
		parkingModelMock.setBaseScenario();
		regularAnalyzer = parkingModelMock.getRegularAnalyzer();
		fileDetector = parkingModelMock.getFileDetector();
		accessControl = parkingModelMock.getAccessControl();
	}

	@Test
	public void testCA1() {
		// given
		Mockito.when(regularAnalyzer.analyze(parkingModelMock.getImageCarA2()))
				.thenReturn(new AnalysisResult(ParkingModelMock.LICENSE_A2, ParkingModelMock.VALID_ACCURACY));

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		Mockito.verify(accessControl).toggleBarrier();
	}

	@Test
	public void testCA2() {
		// given
		Mockito.when(this.regularAnalyzer.analyze(parkingModelMock.getImageCarA1()))
				.thenThrow(new AnalyzeCorruptImageException("corrupted.jpg"));

		// when
		String error = fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		assertEquals("File corrupted.jpg is corrupted.", error);
	}

	@Test
	public void testCA3() {
		// given
		Mockito.when(this.regularAnalyzer.analyze(parkingModelMock.getImageCarA1()))
				.thenReturn(new AnalysisResult(ParkingModelMock.LICENSE_A2, ParkingModelMock.INVALID_ACCURACY));

		// when
		String error = fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		assertEquals("The analyzed image is not recognizable.", error);
	}

	@Test
	public void testCA4() {
		// given
		Mockito.when(this.regularAnalyzer.analyze(parkingModelMock.getImageCarA1()))
				.thenReturn(new AnalysisResult("", ParkingModelMock.ZERO_ACCURACY));

		// when
		String error = fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		assertEquals("The analyzed image is not recognizable.", error);
	}
}
