package AcceptanceTests;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import parking.detector.image.ImageChangeDetector;
import parking.filter.Source;
import parking.recognizer.analyzer.Analyzer;

public class UserStory4 {
	private Source<Image> fileDetector;
	private Analyzer licenseAnalyzer;
	private ImageChangeDetector imageChangeDetector;
	private ParkingModelMock parkingModelMock;

	@Before
	public void setup() {
		parkingModelMock = new ParkingModelMock();
		parkingModelMock.loadImages();
		parkingModelMock.setBaseScenario();

		fileDetector = parkingModelMock.getFileDetector();
		imageChangeDetector = parkingModelMock.getImageChangeDetector();
		licenseAnalyzer = parkingModelMock.getRegularAnalyzer();
	}

	@Test
	public void testCA1() {
		// give
		imageChangeDetector.setLastImage(parkingModelMock.getImageNoCar());

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		// verifies that licenseAnalyzer.analyze() is called only once
		Mockito.verify(licenseAnalyzer, times(1)).analyze(parkingModelMock.getImageCarA2());
	}

	@Test
	public void testCA2() {
		// give
		imageChangeDetector.setLastImage(parkingModelMock.getImageCarA2());

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		// verifies that licenseAnalyzer.analyze() is never called
		Mockito.verify(licenseAnalyzer, never()).analyze(Mockito.any());
	}

	@Test
	public void testCA3() {
		// give
		imageChangeDetector.setLastImage(parkingModelMock.getImageNoCar());

		// when
		fileDetector.process(parkingModelMock.getImageNoCar());

		// then
		// verifies that licenseAnalyzer.analyze() is never calledd
		Mockito.verify(licenseAnalyzer, never()).analyze(Mockito.any());
	}

}
