package AcceptanceTests;

import java.awt.Image;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import parking.access.AccessControl;
import parking.filter.Source;
import parking.site.ParkingSiteImpl;

public class UserStory1 {

	private Source<Image> fileDetector;
	private AccessControl accessControl;
	private ParkingSiteImpl parking;
	private ParkingModelMock parkingModelMock;

	@Before
	public void setup() {
		parkingModelMock = new ParkingModelMock();
		parkingModelMock.loadImages();
		parkingModelMock.setBaseScenario();

		fileDetector = parkingModelMock.getFileDetector();
		accessControl = parkingModelMock.getAccessControl();
		parking = parkingModelMock.getParking();
	}

	@Test
	public void testCA1() {
		// when
		fileDetector.process(parkingModelMock.getImageCarA1());

		// then
		Mockito.verify(accessControl, Mockito.never()).toggleBarrier();
	}

	@Test
	public void testCA2() {

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		Mockito.verify(accessControl).toggleBarrier();
	}

	@Test
	public void testCA3() {
		// given
		fileDetector.process(parkingModelMock.getImageCarA2());

		// when
		fileDetector.process(parkingModelMock.getImageCarA3());

		// then
		Mockito.verify(accessControl, Mockito.times(1)).toggleBarrier();
	}

	@Test
	public void testCA4() {
		// given
		Mockito.when(this.parking.isEnabled()).thenReturn(false);

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		Mockito.verify(accessControl, Mockito.never()).toggleBarrier();
	}

	@Test
	public void testCA5() {
		// given
		Mockito.when(this.parking.isEnabled()).thenReturn(false);

		// when
		fileDetector.process(parkingModelMock.getImageCarA3());

		// then
		Mockito.verify(accessControl, Mockito.never()).toggleBarrier();

		// given
		Mockito.when(this.parking.isEnabled()).thenReturn(true);

		// when
		fileDetector.process(parkingModelMock.getImageCarA2());

		// then
		Mockito.verify(accessControl, Mockito.times(1)).toggleBarrier();
	}
}
