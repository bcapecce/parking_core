package AcceptanceTests;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.mockito.Mockito;

import parking.access.AccessControl;
import parking.access.AccessServices;
import parking.access.RegisterAccess;
import parking.detector.file.FileDetector;
import parking.detector.image.ImageChangeDetector;
import parking.detector.image.ImageComparator;
import parking.filter.Source;
import parking.notification.MessageGateway;
import parking.recognizer.LicenseFinder;
import parking.recognizer.analyzer.AnalysisResult;
import parking.recognizer.analyzer.Analyzer;
import parking.site.ParkedVehicles;
import parking.site.ParkingSiteImpl;
import parking.validator.ParkingValidator;
import parking.validator.validators.AvailablePlaceValidator;
import parking.validator.validators.CompositeValidator;
import parking.validator.validators.EnabledParkingValidator;
import parking.validator.validators.ParkedValidator;

public class ParkingModelMock {

	private Analyzer regularAnalyzer;
	private Analyzer lowLightAnalyzer;
	public static final double VALID_ACCURACY = .7;
	public static final double INVALID_ACCURACY = .3;
	public static final double ZERO_ACCURACY = 0;
	public static final String LICENSE_A1 = "A1";
	public static final String LICENSE_A2 = "A2";
	public static final String LICENSE_A3 = "A3";

	private ParkedVehicles parkedVehicles;
	private ParkingSiteImpl parking;
	private FileDetector fileDetector;
	private ImageChangeDetector imageChangeDetector;
	private AccessControl accessControl;
	private BufferedImage imageCarA1 = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
	private BufferedImage imageCarA2 = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
	private BufferedImage imageCarA3 = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);
	private BufferedImage imageNocar = new BufferedImage(10, 10, BufferedImage.TYPE_INT_RGB);

	public ParkingModelMock() {
		this.parkedVehicles = Mockito.mock(ParkedVehicles.class);
		this.parking = Mockito.mock(ParkingSiteImpl.class);

		AvailablePlaceValidator availablePlaceValidator = new AvailablePlaceValidator(parking);
		EnabledParkingValidator enabledParkingValidator = new EnabledParkingValidator(parking);
		ParkedValidator parkedValidator = new ParkedValidator(parkedVehicles);

		accessControl = Mockito.mock(AccessControl.class);
		Mockito.when(accessControl.toggleBarrier()).thenReturn(true);

		RegisterAccess registerAccess = new RegisterAccess(parkedVehicles, parking);
		AccessServices accessService = new AccessServices(accessControl, registerAccess);

		CompositeValidator compositeValidation = new CompositeValidator(
				Arrays.asList(availablePlaceValidator, enabledParkingValidator, parkedValidator));

		MessageGateway notifier = Mockito.mock(MessageGateway.class);

		ParkingValidator parkingProcessor = new ParkingValidator(accessService, compositeValidation, notifier);
		this.regularAnalyzer = Mockito.mock(Analyzer.class);
		this.lowLightAnalyzer = Mockito.mock(Analyzer.class);
		LicenseFinder licenseFinder = new LicenseFinder(parkingProcessor,
				Arrays.asList(this.regularAnalyzer, this.lowLightAnalyzer), ".7");
		this.imageChangeDetector = new ImageChangeDetector(licenseFinder, new ImageComparator(), "25");

		this.fileDetector = new FileDetector(imageChangeDetector, "", "10000");

	}

	public void loadImages() {
		String path = System.getProperty("user.dir") + "/src/test/resources/images/";
		try {
			imageCarA1 = ImageIO.read(new File(path + "carA1.jpeg"));
			imageCarA2 = ImageIO.read(new File(path + "carA2.jpeg"));
			imageCarA3 = ImageIO.read(new File(path + "nocar.jpeg"));
			imageNocar = ImageIO.read(new File(path + "nocar.jpeg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setBaseScenario() {

		Mockito.when(this.regularAnalyzer.analyze(imageCarA1))
				.thenReturn(new AnalysisResult(LICENSE_A1, VALID_ACCURACY));
		Mockito.when(this.lowLightAnalyzer.analyze(imageCarA1))
				.thenReturn(new AnalysisResult(LICENSE_A1, INVALID_ACCURACY));
		Mockito.when(this.regularAnalyzer.analyze(imageCarA2))
				.thenReturn(new AnalysisResult(LICENSE_A2, VALID_ACCURACY));
		Mockito.when(this.regularAnalyzer.analyze(imageCarA3))
				.thenReturn(new AnalysisResult(LICENSE_A3, VALID_ACCURACY));

		Mockito.when(this.parking.isEnabled()).thenReturn(true);
		Mockito.when(this.parking.hasPlaceAvailable()).thenReturn(true);
		Mockito.when(this.parkedVehicles.licenseExists(LICENSE_A1)).thenReturn(true);
		Mockito.when(this.parkedVehicles.licenseExists(LICENSE_A3)).thenReturn(true);

	}

	public Source<Image> getFileDetector() {
		return this.fileDetector;
	}

	public Analyzer getRegularAnalyzer() {
		return this.regularAnalyzer;
	}

	public Analyzer getLowLightAnalyzer() {
		return this.lowLightAnalyzer;
	}

	public AccessControl getAccessControl() {
		return this.accessControl;
	}

	public ParkingSiteImpl getParking() {
		return this.parking;
	}

	public ImageChangeDetector getImageChangeDetector() {
		return imageChangeDetector;
	}

	public BufferedImage getImageCarA1() {
		return imageCarA1;
	}

	public BufferedImage getImageCarA2() {
		return imageCarA2;
	}

	public BufferedImage getImageNoCar() {
		return imageNocar;
	}

	public BufferedImage getImageCarA3() {
		return imageCarA3;
	}
}
